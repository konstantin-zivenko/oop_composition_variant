class AddressBook:
    def __init__(self):
        self._employee_addresses = {
            1: Address('111 Shevchenko Rd.', 'Dnipro', 'Ukraina', '49009'),
            2: Address('65 Yavornitskogo St.', 'Dnipro', 'Ukraina', '49001'),
            3: Address('23-a Balakireva St.', 'Kharkov', 'Ukraina', '61023', 'Derevianko 1-A'),
            4: Address('24/4 Morskaja St.', 'Melitopol', 'Ukraina', '72308'),
            5: Address('11/34 Tsentralnaja St.', 'Kherson', 'Ukraina', '73009'),
        }

    def get_employee_address(self, employee_id):
        address = self._employee_addresses.get(employee_id)
        if not address:
            raise ValueError(employee_id)
        return address

class Address:
    def __init__(self, street, city, state, zipcode, street2=''):
        self.street = street
        self.street2 = street2
        self.city = city
        self.state = state
        self.zipcode = zipcode

    def __str__(self):
        lines = [self.street]
        if self.street2:
            lines.append(self.street2)
        lines.append(f'{self.city}, {self.state} {self.zipcode}')
        return '\n'.join(lines)
